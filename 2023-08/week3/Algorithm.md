# Algorithm

## 595. 大的国家

https://leetcode.cn/problems/big-countries/?envType=study-plan-v2&envId=30-days-of-pandas&lang=pythondata

### idea

panda df loc多条件
对 特定 列的数据进行逻辑筛选

## yong-liang-ge-zhan-shi-xian-dui-lie-lcof

> 用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead ，分别完成在队列尾部插入整数和在队列头部删除整数的功能。(若队列中没有元素，deleteHead 操作返回 -1 )

## he-bing-liang-ge-pai-xu-de-lian-biao-lcof

    剑指 Offer 25. 合并两个排序的链表

> 输入两个递增排序的链表，合并这两个链表并使新链表中的节点仍然是递增排序的。

21
