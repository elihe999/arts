﻿using System.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using PaddleOCRSharp;

namespace CsharpPaddle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            PaddleOCRSharp.OCRModelConfig config = null;
            config = new OCRModelConfig();
            string root = PaddleOCRSharp.EngineBase.GetRootDirectory();
            string modelPathroot = root + @"\inference";

            Console.WriteLine(modelPathroot);
            config.det_infer = modelPathroot + @"\ch_PP-OCRv4_det_infer";
            config.cls_infer = modelPathroot + @"\ch_ppocr_mobile_v2.0_cls_infer";
            config.rec_infer = modelPathroot + @"\ch_PP-OCRv4_rec_infer";
            config.keys = modelPathroot + @"\ppocr_keys.txt";
            //使用默认参数
            PaddleOCRSharp.OCRParameter oCRParameter = new PaddleOCRSharp.OCRParameter();
            //识别结果对象
            PaddleOCRSharp.OCRResult ocrResult = new PaddleOCRSharp.OCRResult();
            //建议程序全局初始化一次即可，不必每次识别都初始化，容易报错。     
            PaddleOCRSharp.PaddleOCREngine engine = new PaddleOCRSharp.PaddleOCREngine(config, oCRParameter);
            {
                ocrResult = engine.DetectText(@"D:\2b827.png");
            }
            if (ocrResult != null) Console.WriteLine(ocrResult.Text);
        }
    }
}
