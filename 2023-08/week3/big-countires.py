import pandas as pd

data = {'name' : pd.Series(['Afghanistan', 'Albania', 'Algeria', 'Andorra', 'Angola']),
        'continent' : pd.Series(['Asia', 'Europe', 'Africa', 'Europe', 'Africa']),
        'area' : pd.Series([652230, 28748, 2381741, 468, 1246700]),
        'population' : pd.Series([25500100, 2831741, 37100000, 78115, 20609294]),
        'gdp' : pd.Series([20343000000, 12960000000, 188681000000, 3712000000, 100990000000]),
     }


def big_countries(world: pd.DataFrame) -> pd.DataFrame:
    return world.loc[(world['population'] >= 25000000) | (world['area'] >= 3000000), ['name', 'population', 'area']]

