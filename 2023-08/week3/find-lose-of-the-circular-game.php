<?php

class Solution {

    /**
     * @param Integer $n
     * @param Integer $k
     * @return Integer[]
     */
    function circularGameLosers($n, $k) {
        $visit = [];
        $j = 0;
        for ($i = $k; empty($visit[$j]); $i += $k) {
            $visit[$j] = 1;
            $j = ($j + $i) % $n;
        }
        $ans = [];
        foreach (range(0, $n-1) as $i) {
            if (!$visit[$i]) {
                array_push($ans, $i+1);
            }
        }
        return $ans;
    }
}

