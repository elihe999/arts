<?php

class CQueue {
    public $stack_1 = [];
    public $stack_2 = [];
    /**
     */
    function __construct() {

    }

    /**
     * @param Integer $value
     * @return NULL
     */
    function appendTail($value) {
        $this->stack_1[] = $value;
    }

    /**
     * @return Integer
     */
    function deleteHead() {
        if (empty($this->stack_1)) {
            return -1;
        }
        return array_shift($this->stack_1);
    }
}

/**
 * Your CQueue object will be instantiated and called as such:
 * $obj = CQueue();
 * $obj->appendTail($value);
 * $ret_2 = $obj->deleteHead();
 */

$obj = new CQueue();
$obj->appendTail(1);
$ret_2 = $obj->deleteHead();
print_r($ret_2);
$ret_2 = $obj->deleteHead();
print_r($ret_2);
