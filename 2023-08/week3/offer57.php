<?php
class CQueue {
    public $stack_1 = [];
    public $stack_2 = [];
    /**
     */
    function __construct() {

    }

    /**
     * @param Integer $value
     * @return NULL
     */
    function appendTail($value) {
        $this->stack_1[] = $value;
    }

    /**
     * @return Integer
     */
    function deleteHead() {
        $id = null;
        if (!empty($this->stack_2)) {
            return array_pop($this->stack_2);
        } else {
            while (!empty($this->stack_1)) {
                $this->stack_2[] = array_pop($this->stack_1);
            }
            return !empty($this->stack_2) ? array_pop($this->stack_2) : -1;
        }
    }
}

/**
 * Your CQueue object will be instantiated and called as such:
 * $obj = CQueue();
 * $obj->appendTail($value);
 * $ret_2 = $obj->deleteHead();
 */

class CQueueSPL {

    public $stack1 = null;
    public $stack2 = null;

    /**
     */
    function __construct() {
        $this->stack1 = new SplStack();
        $this->stack2 = new SplStack();
    }

    /**
     * @param Integer $value
     * @return NULL
     */
    function appendTail($value) {
        return $this->stack1->push($value);
    }

    /**
     * @return Integer
     */
    function deleteHead() {
        if(!$this->stack2->isEmpty()){
            return $this->stack2->pop();
        }else{
            while(!$this->stack1->isEmpty()){
                $this->stack2->push($this->stack1->pop());
            }
            return !$this->stack2->isEmpty() ? $this->stack2->pop() : -1;
        }
    }
}

/**
 * Your CQueue object will be instantiated and called as such:
 * $obj = CQueue();
 * $obj->appendTail($value);
 * $ret_2 = $obj->deleteHead();
 */

#作者：归子莫
#链接：https://leetcode.cn/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof/solutions/945727/phpti-jie-by-guizimo-5m24/
#来源：力扣（LeetCode）
#著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
