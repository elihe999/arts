<?php

// 8.0
// array_is_list([]); // true
// array_is_list(['apple', 2, 3]); // true
// array_is_list([0 => 'apple', 'orange']); // true

// // key 未从 0 开始
// array_is_list([1 => 'apple', 'orange']); // false

// // key 的顺序不正确
// array_is_list([1 => 'apple', 0 => 'orange']); // false

// // 包含非整数 key
// array_is_list([0 => 'apple', 'foo' => 'bar']); // false

// // 非连续 key
// $re = array_is_list([0 => 'apple', 2 => 'bar']); // false

$array = ['a' => 1, 'b' => 2, 'c' => 3];

$firstKey = array_key_first($array);

var_dump($firstKey);

$keys = array('foo', 5, 10, 'bar');
$a = array_fill_keys($keys, 'banana');
print_r($a);

// array_multisort
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

$data[] = array('volume' => 67, 'edition' => 2);
$data[] = array('volume' => 86, 'edition' => 1);
$data[] = array('volume' => 85, 'edition' => 6);
$data[] = array('volume' => 98, 'edition' => 2);
$data[] = array('volume' => 86, 'edition' => 6);
$data[] = array('volume' => 67, 'edition' => 7);

// Pass the array, followed by the column names and sort flags
$sorted = array_orderby($data, 'volume', SORT_DESC, 'edition', SORT_ASC);

var_dump($sorted);