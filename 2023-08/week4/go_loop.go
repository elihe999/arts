package main
 
import (  
    "fmt"
)
 
func main() {  
    for i := 1; i <= 10; i++ {
        fmt.Printf(" %d",i)
    }
	for i := 1; i <= 10; i++ {
        if i > 5 {
            break // 如果 i > 5 就跳出
        }
        fmt.Printf("%d ", i)
    }
    fmt.Printf("\nline after for loop")
	for i := 1; i <= 10; i++ {
        if i%2 == 0 {
            continue
        }
        fmt.Printf("%d ", i)
    }
	age := 7

    if age > 6 {
        fmt.Println("good")
    }
	switch age {
    case 5:
        fmt.Println("The age is 5")
    case 7:
        fmt.Println("The age is 7")
    case 10:
        fmt.Println("The age is 10")
    default:
        fmt.Println("The age is unkown")
	}
}