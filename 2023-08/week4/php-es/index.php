<?php

require 'vendor/autoload.php';

use Elasticsearch\ClientBuilder;

$client = ClientBuilder::create()->build();

# 索引一个文档
# Version 7.11
$params = [
    'index' => 'my_index',
    'id' => 'my_id',
    'body' => ['testField' => 'abc']
];

$response = $client->index($params);
print_r($response);

# 获取一个文档
# Version 7.11
$params = [
    'index' => 'my_index',
    'id' => 'my_id'
];

$response = $client->get($params);
print_r($response);

# 搜索一个文档
# Version 7.11
$params = [
    'index' => 'my_index',
    'type' => 'my_type',
    'body' => [
        'query' => [
            'match' => [
                'testField' => 'abc'
            ]
        ]
    ]
];

$response = $client->search($params);
print_r($response);