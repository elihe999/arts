<?php
trait Instance
{
    static private $instance;
    /**
     * 防止被外部new类
     */
    private function __construct()
    {
    }
    /**
     * 防止被继承者克隆
     */
    private function __clone()
    {
    }

    static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }
}

class Test
{
    use Instance;

    private $config;

    function set($index, $value)
    {
        $this->config[$index] = $value;
    }

    function get($index)
    {
        var_dump($this->config[$index]);
    }
}

$model = Test::getInstance();
$model->set("index", "设置index参数值");
$models = Test::getInstance();
$models->get("index");
