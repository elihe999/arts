<?php

// 第一周
print_r(date("w", strtotime(date('Y').'W01')) . PHP_EOL);
// 上一个月
$firstOfThisMonth = date('Y-m') . '-01';
echo date('m', strtotime($firstOfThisMonth . ' -1 month'));
// first
$dt1 = strtotime("today");
$month = strtotime("first day of this month", $dt1);
echo "\n\nToday is: " . date( 'Y-m-d', $dt1 );
echo  "\nFirst day of last month: " . date("w", $month);