<?php

echo titleToNumber('M');
function titleToNumber($columnTitle) {
    $ans = 0;
    $title_len = strlen($columnTitle);
    for ($i = 0;$i<$title_len;$i++) {
        $column_no = ord($columnTitle[$i]) - 64;
        $ans += 26 ** ($title_len - $i - 1) * $column_no;
    }

    return $ans;
}
