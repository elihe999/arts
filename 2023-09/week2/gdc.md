# Greatest Common Divisor

- 最大公约数

1.原理
    GCD算法是用于求解最大公约数的方法，利用了欧几里得算法，即辗转相除法。

    最重要的等式莫过于（核心中的核心）：gcd(a,b) = gcd(b,a mod b) (不妨设a>b 且r=a mod b ,r不为0)


证法一
    a可以表示成a = kb + r（a，b，k，r皆为正整数，且r<b），则r = a mod b

    假设d是a,b的一个公约数，记作d|a,d|b，即a和b都可以被d整除。

    而r = a - kb，两边同时除以d，r/d=a/d-kb/d=m，由等式右边可知m为整数，因此d|r

    因此d也是b,a mod b的公约数

    假设d是b,a mod b的公约数, 则d|b,d|(a-k*b),k是一个整数。

    进而d|a.因此d也是a,b的公约数

    因此(a,b)和(b,a mod b)的公约数是一样的，其最大公约数也必然相等，得证。


证法二
    第一步：令c=gcd(a,b)，则设a=mc，b=nc

    第二步：可知r =a-kb=mc-knc=(m-kn)c

    第三步：根据第二步结果可知c也是r的因数

    第四步：可以断定m-kn与n互素【否则，可设m-kn=xd,n=yd,(d>1)，则m=kn+xd=kyd+xd=(ky+x)d，则a=mc=(ky+x)dc，    b=nc=ycd，故a与b最大公约数≥cd，而非c，与前面结论矛盾】

    从而可知gcd(b,r)=c，继而gcd(a,b)=gcd(b,r)，得证

    注意:两种方法是有区别的。

2.普通方法
# 最大公因数普通算法
```
int gcd(int m,int n)
{    
    int t,r;    
    if (m<n)//为了确保是大数除小数    
    {        
        t=m;        
        m=n;       
        n=t;    
    }    
 
    while((m%n)!=0)//辗转相除    
    {        
        r=m%n;        
        m=n;        
        n=r;    
    }   
 
    return n;
}
```

3.递归算法
```
//求最大公因数递归算法
int gcd(int x, int y)
{	
    if (y)			
        return gcd(y, x%y);		
    else			
        return x;
} 
```

4.最美妙算法
   注：巧用位运算，尤其是异或，会带来意想不到的方法。

# 最美妙的最大公因数算法
int gcd(int x, int y)
{
    while(y^=x^=y^=x%=y);
    return x;
}
  怎么玩的呢？

  我来告诉你：核心不过是gcd(a,b) = gcd(b,a mod b)

  过程如下：

# 首先声明^就是异或，a^a=0，a^0=a其次连等是从右往左结合，不要忘了哦！
x1=x%y;
y1=y^x1=y^(x%y)
x2=x1^y1=(x%y)^y^(x%y)=y
y2=y1^x2=y^(x%y)^y=x%y
即：
x'=y, y'=x%y
gcd(x,y) = gcd(y,x%y) = gcd(x',y')
  
————————————————
版权声明：本文为CSDN博主「wzx15927662183」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/wzx15927662183/article/details/90212839