<?php

class Solution {

    /**
     * @param String $word1
     * @param String $word2
     * @return String
     */
    function mergeAlternately($word1, $word2) {
        $len = max(strlen($word1), strlen($word2));
        $result = '';
        foreach(range(0, $len) as $i) {
            if (isset($word1[$i])) {
                $result .= $word1[$i];
            }
            if (isset($word2[$i])) {
                $result .= $word2[$i];
            }
        }
        return $result;
    }
}