<?php

class Solution {

    /**
     * @param Integer[] $nums1
     * @param Integer[] $nums2
     * @return Integer
     */
    function minNumber($nums1, $nums2) {
        $arr1Min = min($nums1);
        $arr2Min = min($nums2);
        $intersctNumber = array_intersect($nums1, $nums2);
        if (!empty($intersctNumber)) {
            return min($intersctNumber);
        } else {
            $tens = min($arr1Min, $arr2Min);
            $units = max($arr1Min, $arr2Min);
            return $tens * 10 + $units;
        }
    }
}
$nums1 = [3,5,2,6];
$nums2 = [3,1,7];
$test = new Solution();
$res = $test->minNumber($nums1, $nums2);
print_r($res);