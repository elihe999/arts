package main
import (
    "fmt"
)
func main() {
	channel1 := make(chan string)
	channel2 := make(chan string)
	channel3 := make(chan string)
	//
	// channel1 <- "hello"
	// channel2 <- "hello"
	value := "123"
	// deadlock
	defer close(channel1)
	defer close(channel2)
	defer close(channel3)
	go func() {
		channel1 <- "hello"
		channel2 <- "hello"
		channel3 <- "hello"
		select {
			case <- channel1:
			// 执行的代码
				fmt.Println("Chan1")
			case value := <- channel2:
			// 执行的代码
				fmt.Println("Chan2")
				fmt.Println(value)

			case channel3 <- value:
			// 执行的代码

				fmt.Println("Chan3")
				// 你可以定义任意数量的 case

			default:
				fmt.Println("default")
				// 所有通道都没有准备好，执行的代码
		}
	}()
	
	fmt.Println(value)
}