<?php 


function findCommonParent($string1, $string2) {
    $units1 = explode('-', $string1);
    $units2 = explode('-', $string2);

    $parents1 = array();
    $parents2 = array();

    // 移除根节点
    array_shift($units1);
    array_shift($units2);

    // 构建每个单位的父级数组
    foreach ($units1 as $unit) {
        $parents1[] = $unit;
    }

    foreach ($units2 as $unit) {
        $parents2[] = $unit;
    }

    // 查找公共的上级单位
    $commonParents = array_intersect($parents1, $parents2);

    return !empty($commonParents) ? implode('-', $commonParents) : null;
}

print_r(findCommonParent('0-4-6', '0-1-2-3'));
?>