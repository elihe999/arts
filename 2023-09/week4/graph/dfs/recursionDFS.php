<?php

function dfs($graph, $start, $visited = [])  
{  
    // 标记当前节点为已访问  
    $visited[$start] = true;  
    echo $start . " ";  
  
    // 遍历当前节点的所有邻居节点  
    foreach ($graph[$start] as $neighbor) {  
        // 如果邻居节点未被访问，则递归调用 dfs 函数  
        if (empty($visited[$neighbor])) {  
            dfs($graph, $neighbor, $visited);  
        }  
    }  
}  
  
// 示例图的邻接表表示  
$graph = [  
    0 => [1, 2],  
    1 => [2],  
    2 => [0, 3],  
    3 => [3]  
];  
  
// 从节点 2 开始进行深度优先搜索  
dfs($graph, 2);