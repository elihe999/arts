<?php  
  
function dfs($graph, $start)  
{  
    $visited = [];  
    $stack = [];  
    $stack[] = $start;  
  
    while (!empty($stack)) {  
        $node = array_pop($stack);  
  
        // 标记当前节点为已访问  
        $visited[$node] = true;  
        echo $node . " ";  
  
        // 将当前节点的未访问邻居节点入栈  
        foreach ($graph[$node] as $neighbor) {  
            if (empty($visited[$neighbor])) {  
                $stack[] = $neighbor;  
            }  
        }  
    }  
}  
  
// 示例图的邻接表表示  
$graph = [  
    0 => [1, 2],  
    1 => [2],  
    2 => [0, 3],  
    3 => [3]  
];  
  
// 从节点 2 开始进行深度优先搜索  
dfs($graph, 2);