<?php
function isParentUnit($unitString1, $unitString2) {
    if (strlen($unitString1) > strlen($unitString2)) {
        $units1 = explode('-', $unitString1);
        $units2 = explode('-', $unitString2);
    } else {
        $units1 = explode('-', $unitString2);
        $units2 = explode('-', $unitString1);
    }

    // 移除根节点
    array_shift($units1);
    array_shift($units2);

    // 检查 $unitString1 是否是 $unitString2 的上级单位
    $isParent = true;
    foreach ($units2 as $unit) {
        if (!in_array($unit, $units1)) {
            $isParent = false;
            break;
        }
    }

    return $isParent;
}

// 示例用法
$unitString1 = '-0-1'; // 单位1的人的表达方式
$unitString2 = '-0-1-3-5-6-9'; // 单位1的下一级单位的表达方式
$isParent = isParentUnit($unitString1, $unitString2);
if ($isParent) {
    echo "$unitString1 是 $unitString2 的上级单位";
} else {
    echo "$unitString1 不是 $unitString2 的上级单位";
}

echo PHP_EOL;
// 示例用法
$unitString1 = '-0-1'; // 单位1的人的表达方式
$unitString2 = '-0-1'; // 单位1的下一级单位的表达方式
$isParent = isParentUnit($unitString1, $unitString2);
if ($isParent) {
    echo "$unitString1 是 $unitString2 的上级单位";
} else {
    echo "$unitString1 不是 $unitString2 的上级单位";
}
?>