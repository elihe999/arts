<?php
class Solution {

    /**
     * @param Integer[] $nums
     * @return NULL
     */
    function moveZeroes(&$nums) {
        $slowIndex = 0;
        for ($fastIndex = 0; $fastIndex < count($nums); $fastIndex++) {
            if ($nums[$fastIndex] != 0) {
                list($nums[$slowIndex], $nums[$fastIndex]) = [$nums[$fastIndex], $nums[$slowIndex]];
                $slowIndex++;
            }
        }
    }
}